import '../scss/styles';
import app from './classes/App';
import '../../dist/semantic.js';
import {Reminder} from "./classes/Reminder";
app.init();

(<any>$('.ui.accordion'))
    .accordion()
;

app.$form.addEventListener('submit', function ( event ) {

    event.preventDefault();

    const title = app.$title.value;
    const description = app.$description.value;

    const date_debut = app.$date_debut.value;
    const date_fin = app.$date_fin.value;

    if ( title.length > 0 && description.length > 0 ) {

        const reminder = new Reminder(title, description, date_debut, date_fin);

        app.add( reminder );

        app.save();

        let r = date_debut.split('-');
        let v = date_fin.split('-');

        let year = parseInt(r[0]);
        let day = parseInt(r[2]);
        let month = parseInt(r[1]) - 1;

        let year_fin = parseInt(v[0]);
        let day_fin = parseInt(v[2]);
        let month_fin = parseInt(v[1]) - 1;

        let date = new Date(year, month, day).toLocaleString();
        let date_de_fin = new Date(year_fin, month_fin, day_fin).toLocaleString();
        let date_debut_3 = new Date(year, month, day - 6).toLocaleString();

        reminder.render( false,
                        app.$list_to_day,
                        app.$list_3_day,
                        app.$alert_container,
                        app.$container,
                        date, date_de_fin, date_debut_3 );
    }
});

app.$icon_close.addEventListener('click', function ( event ) {
    app.$alert_container.className = 'ui info message hidden';
});

app.$reload_btn.addEventListener('click', function (event) {
    location.reload();
});

app.$delete_btn.addEventListener('click', function (event) {

    for ( let reminder of app.reminders) {

                if (reminder.deletable) {

                    let key = app.reminders.indexOf(reminder);

                     app.delete(key);
                     app.save();
                }
    }
    location.reload();
});

