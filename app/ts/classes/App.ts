import {Reminder} from "./Reminder";

class App {

// <editor-fold desc="My Property of App">
    public reminders: Array<Reminder>;

    public $container: HTMLDivElement;
    public $form: HTMLFormElement;
    public $title: HTMLInputElement;
    public $description: HTMLTextAreaElement;
    public $date_debut: HTMLInputElement;
    public $date_fin: HTMLInputElement;
    public $alert_container: HTMLDivElement;
    public $list_3_day: HTMLUListElement;
    public $list_to_day: HTMLUListElement;
    public $icon_close: HTMLElement;
    public $view_delete_btn: HTMLElement;
    private $all_container: HTMLElement;
    private boolean: boolean;
    public $reload_btn: HTMLButtonElement;
    public $delete_btn: HTMLButtonElement;
// </editor-fold>

    constructor() {

        this.reminders = [];

        this.$container = <HTMLDivElement>document.getElementById('container');

        this.$alert_container = <HTMLDivElement>document.getElementById('alert-container');
        this.$icon_close = <HTMLElement>document.getElementById('close');
        this.$list_to_day = <HTMLUListElement>document.getElementById('list-to-day');
        this.$list_3_day = <HTMLUListElement>document.getElementById('list-3-day');
        
        this.$form = <HTMLFormElement>document.getElementById('add-reminder');
        
        this.$reload_btn = <HTMLButtonElement>document.getElementById('reload-btn');
        this.$delete_btn = <HTMLButtonElement>document.getElementById('delete-btn');

        this.$title = <HTMLInputElement>document.getElementById('title');
        this.$description = <HTMLTextAreaElement>document.getElementById('description');
        this.$date_debut = <HTMLInputElement>document.getElementById('date-debut');
        this.$date_fin = <HTMLInputElement>document.getElementById('date-fin');
    }

    init( ) {
        const str_reminders = localStorage.getItem('reminders');

        if ( str_reminders ) {

            const json_reminders = JSON.parse( str_reminders );

            for ( let json_reminder of json_reminders ) {

                    const reminder = new Reminder(json_reminder.title,
                                                json_reminder.description,
                                                json_reminder.date_debut,
                                                json_reminder.date_fin);

                    let r = json_reminder.date_debut.split('-');
                    let v = json_reminder.date_fin.split('-');

                    let year = parseInt(r[0]);
                    let day = parseInt(r[2]);
                    let month = parseInt(r[1]) - 1;

                    let year_fin = parseInt(v[0]);
                    let day_fin = parseInt(v[2]);
                    let month_fin = parseInt(v[1]) - 1;

                    let date_debut = new Date(year, month, day).toLocaleString();
                    let date_fin = new Date(year_fin, month_fin, day_fin).toLocaleString();
                    let date_debut_3 = new Date(year, month, day - 6).toLocaleString();

                    if (!json_reminder.deletable) {
                        this.boolean = true;
                    }

                    reminder.render( this.boolean,
                                    this.$list_to_day,
                                    this.$list_3_day,
                                    this.$alert_container,
                                    this.$container,
                                    date_debut, date_fin, date_debut_3 );

                    this.add( reminder );
            }
        }
    }

    add ( reminder: Reminder ) {

        this.reminders.push( reminder );
        console.log(this.reminders);
    }

    save() {

        const str_reminder = JSON.stringify( this.reminders );
        localStorage.setItem('reminders', str_reminder);
    }

    delete ( position: number ) {

        this.reminders.splice( position, 10000000000 );
    }

}
export default new App();