import app from "./App";

export class Reminder {

//<editor-fold desc="My property of Reminder">
    private title: string;
    private description: string;

    private date_debut: string;
    private date_fin: string;

    private $div: HTMLDivElement;
    private $title: HTMLSpanElement;
    private $date_debut: HTMLSpanElement;
    private $date_fin: HTMLSpanElement;
    private $div_2: HTMLDivElement;
    public $p: HTMLParagraphElement;
    private $div_1: HTMLDivElement;
    private $i: HTMLElement;
    private $br: HTMLBRElement;
    public $orig_div: HTMLDivElement;
    private date: number|Date|string;
    private year: number|Date|string;
    private month: number|Date|string;
    private day: number|Date|string;
    private date_debut_3: string;
    private $i_1: HTMLElement;
    private $ul: HTMLUListElement;
    private $li: HTMLLIElement;
    private $div_header: HTMLDivElement;
    deletable: boolean;
    hidden: string;
    private $view_delete_btn: HTMLButtonElement;
    private $nothing_on_3_day: HTMLElement;
    private $nothing_to_day: HTMLElement;
//</editor-fold>

    constructor(title: string, description: string, date_debut: string, date_fin: string ) {

        this.date = new Date();

        this.year = Number(this.date.getFullYear());
        this.month = Number(this.date.getMonth());
        this.day = Number(this.date.getDate());

        this.$view_delete_btn = <HTMLButtonElement>document.getElementById('view-delete-btn');

        this.date = new Date(this.year, this.month, this.day).toLocaleString();
        this.date_debut_3 = new Date(this.year, this.month, this.day - 3).toLocaleString();

        this.title = title;
        this.description = description;
        this.date_debut = date_debut;
        this.date_fin = date_fin;

        this.deletable = false;
        this.hidden = '';

        this.$nothing_on_3_day = <HTMLElement>document.getElementById('nothing_on_3_day');
        this.$nothing_to_day = <HTMLElement>document.getElementById('nothing_to_day');
    }

    render( boolean: boolean,
            $list_to_day: HTMLUListElement,
            $list_3_day: HTMLUListElement,
            $alert_container: HTMLDivElement,
            $parent: HTMLDivElement,
            date_debut: string,
            date_fin: string,
            date_debut_3: string) {


        this.$orig_div = document.createElement('div');

        this.$i_1 = document.createElement('i');
        this.$i_1.className = "close icon";
        this.$i_1.textContent = 'Événement aujourd\'hui !!';

        this.$div_header = document.createElement('div');
        this.$div_header.className = 'header';

        this.$li = document.createElement('li');
        this.$li.textContent = this.title;

            let v = date_debut_3.split('/');
            let day2 = parseInt(v[0]) + 1;
            let day1 = parseInt(v[0]) + 2;

            let date_debut_2 = day2 + '/' + v[1] + '/' + v[2];
            let date_debut_1 = day1 + '/' + v[1] + '/' + v[2];

            if ( date_debut_3 == this.date_debut_3
                || date_debut_2 == this.date_debut_3
                || date_debut_1 == this.date_debut_3) {

                this.$orig_div.className = 'container ui inverted blue segment';
                $list_3_day.append(this.$li);
                $alert_container.className = 'ui info message';

                this.$nothing_on_3_day.className = 'hidden';
            }

            else if ( date_debut == this.date) {

                this.$orig_div.className = 'container ui inverted green segment';
                $list_to_day.append(this.$li);
                $alert_container.className = 'ui info message';

                this.$nothing_to_day.className = 'hidden';
            }

            else if ( this.date > date_fin ) {
                this.$orig_div.className = 'container ui inverted red segment';
                this.deletable = true;

                this.$orig_div.id = this.hidden;
            }

            else {
                this.$orig_div.className = 'container ui inverted segment ';
            }

        this.$view_delete_btn.addEventListener('click', () => {

            if (!this.deletable) {

                this.$orig_div.style.display = 'none';
                app.$delete_btn.className = 'ui orange button';
            }
        });

        this.$div_1 = document.createElement('div');
        this.$div_1.className = 'ui inverted accordion';

        this.$div = document.createElement('div');
        this.$div.className = 'title';

        this.$title = document.createElement('span');
        this.$title.className = 'title-stock';
        this.$title.textContent = this.title;

        this.$date_debut = document.createElement('span');
        this.$date_debut.className =  'date-debut-stock';
        this.$date_debut.textContent = this.date_debut.toString() + ' TO ';

        this.$date_fin = document.createElement('span');
        this.$date_fin.className =  'date-fin-stock';
        this.$date_fin.textContent = this.date_fin.toString();


        this.$i = document.createElement('i');
        this.$i.className = 'dropdown icon';

        this.$br = document.createElement('br');

        this.$div.append(this.$i, this.$title, this.$br, this.$date_debut, this.$date_fin );

        this.$div_2 = document.createElement('div');
        this.$div_2.className = 'content';

        this.$p = document.createElement('p');
        this.$p.className = 'transition block';
        this.$p.textContent = this.description;

       this.$div_2.append(this.$p);

       this.$div_1.append(this.$div, this.$div_2);

       this.$orig_div.append(this.$div_1);

        this.$div_1.addEventListener('click', () => {

            if ( this.$p.className != 'transition visible')  {

                this.$p.className = 'transition visible';
                this.$div.className = 'title active';
            }

            else if (this.$p.className == 'transition visible') {
                this.$p.className = 'transition';
                this.$div.className = 'title';

            }
        });

       $parent.append(this.$orig_div);
    }
    
}